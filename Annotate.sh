#!/bin/bash

for i in `ls *.vcf.gz`
do
  targ=$i
  snpSift annotate /lustre1/genomes/hg19/annotation/dbSNP-146.vcf.gz $targ | \
  snpSift annotate /lustre1/genomes/hg19/annotation/COSMIC/Cosmic_grch37_cosmic_v73_VCF/CosmicCodingMuts.vcf.gz | \
  snpSift annotate /lustre1/genomes/hg19/annotation/COSMIC/Cosmic_grch37_cosmic_v73_VCF/CosmicNonCodingVariants.vcf.gz | \
  snpSift annotate /lustre1/genomes/hs37d5/annotation/clinvar_20160802.vcf.gz | \
  snpSift dbnsfp -db /lustre1/genomes/hg19/annotation/dbNSFP2.9.txt.gz -g hg19 -collapse - | \
  snpEff -cancer -v -lof GRCh37.75 -dataDir /lustre1/ctgb-usr/local/src/snpEff/data/ | vcfsort| bgzip -f -c > ${targ%%.vcf.gz}.Eff.vcf.gz
done
