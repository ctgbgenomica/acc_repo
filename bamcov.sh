#PBS -N cov
#PBS -l select=1:app=java:ncpus=6:mem=18gb
#PBS -P 161
#PBS -q workq
#PBS -m ae

cd /lustre2/scratch/ezapparoli/ACC/BAM

condactivate
bedtools multicov -D -f 0.1 -bams `ls *1TA*.bam`  -bed CHR7.bed  > CHR7cov2.txt 
condeactivate
