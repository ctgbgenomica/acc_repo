#!/bin/bash

#!/bin/bash
#SBATCH --partition=workq  #workq is the default and unique queue, you do not need to specify.
#SBATCH --account bucci.gabriele
#SBATCH --mem-per-cpu=15GB  # amout of RAM in MB required (and max ram available). Xor "--mem-per-cpu"
#SBATCH --time=INFINITE  ## OR #SBATCH --time=10:00 means 10 minutes OR --time=01:00:00 means 1 hour
#SBATCH --ntasks=12  # nubmer of required cores
#SBATCH --nodes=1  # not really useful for not mpi jobs
#SBATCH --mail-type=FAIL ## BEGIN, END, FAIL or ALL
#SBATCH --mail-user=bucci.gabriele@hsr.it
#SBATCH --error="/home/bucci.gabriele/myjob.err"
#SBATCH --output="/home/bucci.gabriele/myjob.out"
#this script accepts illumina standard fastq names

sleep 1

FA=/beegfs/scratch/ric.cosr/ric.cosr/ACC/tmap-f3/hg19/hg19.fasta
DICT=/beegfs/scratch/ric.cosr/ric.cosr/ACC/tmap-f3/hg19/hg19.dict
BED=/beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed.bed
interval=/beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed_merged.interval_list
gnomad=/beegfs/scratch/ric.cosr/ric.cosr/ACC/somatic-b37/af-only-gnomad.raw.sites_renamed_contigs.vcf
panelexome=/beegfs/scratch/ric.cosr/ric.cosr/ACC/somatic-b37/Mutect2-exome-panel_renamed_contigs.vcf

conda activate accwp3

gatk --java-options "-Xmx10g -Xms10g -Djava.io.tmpdir=/beegfs/scratch/tmp/" GenomicsDBImport -R $FA --sample-name-map normal.sample_map  -L $interval --merge-input-intervals --sequence-dictionary $DICT --genomicsdb-workspace-path pon_db --reader-threads 12 --batch-size 5

exit

