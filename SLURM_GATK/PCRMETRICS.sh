#!/bin/bash

#!/bin/bash 
#SBATCH --partition=workq  #workq is the default and unique queue, you do not need to specify.
#SBATCH --account bucci.gabriele
#SBATCH --mem=15GB  # amout of RAM in MB required (and max ram available). Xor "--mem-per-cpu"
#SBATCH --time=INFINITE  ## OR #SBATCH --time=10:00 means 10 minutes OR --time=01:00:00 means 1 hour
#SBATCH --ntasks=6  # nubmer of required cores
#SBATCH --nodes=1  # not really useful for not mpi jobs
#SBATCH --mail-type=FAIL ## BEGIN, END, FAIL or ALL
#SBATCH --mail-user=bucci.gabriele@hsr.it
#SBATCH --error="/home/bucci.gabriele/myjob.err"
#SBATCH --output="/home/bucci.gabriele/myjob.out"
#this script accepts illumina standard fastq names
R1=$1
sleep 3

FA=/beegfs/scratch/ric.cosr/ric.cosr/ACC/tmap-f3/hg19/hg19.fasta
DICT=/beegfs/scratch/ric.cosr/ric.cosr/ACC/tmap-f3/hg19/hg19.dict
BED=/beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed.bed

conda activate accwp3

gatk CollectTargetedPcrMetrics \
       -I $R1 \
       -O ${R1%%.bam}_pcr_metrics.txt \
       -R $FA \
       --COVERAGE_CAP 2000 \
       --PER_TARGET_COVERAGE ${R1%%.bam}_target_coverage.txt \
       --AMPLICON_INTERVALS /beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed.interval_list \
       --TARGET_INTERVALS /beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed_merged.interval_list



exit

