#!/bin/bash 
#SBATCH --partition=workq  #workq is the default and unique queue, you do not need to specify.
#SBATCH --account bucci.gabriele
#SBATCH --mem-per-cpu=5GB  # amout of RAM in MB required (and max ram available). Xor "--mem-per-cpu"
#SBATCH --time=INFINITE  ## OR #SBATCH --time=10:00 means 10 minutes OR --time=01:00:00 means 1 hour
#SBATCH --ntasks=6 # nubmer of required cores
#SBATCH --nodes=1  # not really useful for not mpi jobs
#SBATCH --mail-type=FAIL ## BEGIN, END, FAIL or ALL
#SBATCH --mail-user=bucci.gabriele@hsr.it
#SBATCH --output=R-%x.%j.out
#SBATCH --error=R-%x.%j.err
#this script accepts illumina standard fastq names
T=$1
sleep 1

FA=/beegfs/scratch/ric.cosr/ric.cosr/ACC/tmap-f3/hg19/hg19.fasta
DICT=/beegfs/scratch/ric.cosr/ric.cosr/ACC/tmap-f3/hg19/hg19.dict
BED=/beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed.bed
interval=/beegfs/scratch/ric.cosr/ric.cosr/ACC/ACC_Lung/BED/WG_IAD97207.20161004.spikeinCL.designed_merged.interval_list
cosmic=/beegfs/scratch/ric.cosr/ric.cosr/ACC/hs37d5/annotation/CosmicCodingMuts.v80.vcf.gz
gnomad=/beegfs/scratch/ric.cosr/ric.cosr/ACC/somatic-b37/af-only-gnomad.raw.sites_renamed_contigs.vcf.gz
panel=/beegfs/scratch/ric.cosr/ric.cosr/ACC/somatic-b37/Mutect2-WGS-panel-b37.vcf
panelexome=/beegfs/scratch/ric.cosr/ric.cosr/ACC/somatic-b37/Mutect2-exome-panel_renamed_contigs.vcf.gz
blacklist=/beegfs/scratch/ric.cosr/ric.cosr/ACC/somatic-b37/blacklist.hg19.list


conda activate accwp3

gatk GetSampleName -I BAM/$T -O BAM/${T%%.bam}.name

Tname=$(cat BAM/${T%%.bam}.name)

gatk FilterMutectCalls \
    -V calls/${Tname}_somatic_m2.vcf.gz \
    -R $FA \
    --filtering-stats calls/${Tname}_filter.stats \
    -L $interval \
    --max-events-in-region 3 \
    -O calls/${Tname}_somatic_oncefiltered.vcf.gz \
    --ob-priors calls/${Tname}_read-orientation-model.tar.gz



exit

