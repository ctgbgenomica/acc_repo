for i in `ls lung14????-1T*bam`
do
  Sample=${i%%-1T*}
  Tumor=$i
  Normal=`ls ${i%%-1T*}-2N*bam`
  echo $Tumor
  echo $Normal
  freebayes -f /lustre1/genomes/hg19/fa/hg19.fa -F 0.01 -C 2 -k -w -V -a  --strict-vcf --pooled-continuous  --read-snp-limit 10 --min-base-quality 20 --min-mapping-quality 1 --read-max-mismatch-fraction 0.2 -Y 1 -R 1 --haplotype-length 0 -O $Tumor $Normal -v  ${Sample}_T-N.vcf
done
