#!/bin/bash

targgz=$1
targ=${targgz%%.gz}
name=${targ%_*}
tumor=${name}
normal=${tumor/-1T/-2N}

script_name=${targ%_*}.sh


cat <<__EOF__> $script_name

#PBS -l select=1:app=java:ncpus=4:mem=14gb
#PBS -P 161 
#PBS -q workq
###PBS -M bucci.gabriele@hsr.it
#PBS -m ae
#PBS -N ${script_name:0:5}

cd $PWD 

condactivate freebayes
 
gunzip -c $targgz > $targ

<<<<<<< HEAD
/lustre2/scratch/gbucci/vcf2maf-master/vcf2maf.pl --input-vcf  ${targ} --output-maf ${tumor}.maf \
=======
perl /lustre2/scratch/gbucci/vcf2maf-master/vcf2maf.pl --input-vcf  ${targ} --output-maf ${tumor}.maf \
>>>>>>> origin
--maf-center CTGB --tumor-id $tumor --normal-id $normal \
--ref-fasta /home/gbucci/.vep/homo_sapiens/90_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa \
--species homo_sapiens --ncbi-build GRCh37 --cache-version 90

rm $targ
##bgzip $targ 
##tabix -f $targgz
condeactivate

__EOF__

qsub  $script_name
