#PBS -N CNVBASELINE
#PBS -l select=1:app=java:ncpus=11:mem=18gb
#PBS -P 161
#PBS -q workq
#PBS -m ae

cd /lustre2/scratch/ezapparoli/ACC/BAM

condactivate


cnvkit.py batch -p 10 --normal lung146WIS-2NAD114.bam lung147GRW-2NAD114.bam lung147SAZ-2NAD114.bam lung14BI52-2NAD114.bam lung14C0WF-2NAD113.bam lung14DOWR-2NAD114.bam lung14DYS3-2NAD114.bam lung14FCHM-2NAD114.bam lung14G5M5-2NAD114.bam lung14HDL7-2NAD114.bam lung14IRKS-2NAD114.bam lung14J1RZ-2NAD114.bam lung14MP0O-2NAD114.bam lung14MV9G-2NAD114.bam lung14NYDM-2NAD114.bam lung14XXNX-2NAD114.bam     --targets /lustre2/scratch/ezapparoli/ACC/BAM/target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.target.bed --fasta /lustre1/genomes/hg19/fa/hg19.fa --drop-low-coverage --short-names --annotate /lustre1/genomes/hg19/annotation/hg19.refFlat.txt     --output-reference /lustre2/scratch/ezapparoli/ACC/BAM/acc_normal/acc.hg19.cnn --diagram --scatter --output-dir /lustre2/scratch/ezapparoli/ACC/BAM/acc_normal/

condeactivate
