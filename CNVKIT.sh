#CNVKIT ACC
cd /lustre2/scratch/ezapparoli/ACC/BAM/

ls *bam|tee|grep -v 1TB|sort > DNABAM.txt

#DONOR_BASELINE
#ftp://hgdownload.soe.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/
cnvkit.py access /lustre1/genomes/hg19/fa/hg19.fa -x wgEncodeDukeMapabilityRegionsExcludable.bed -o access-excludes.hg19.bed
#cnvkit.py antitarget /lustre2/scratch/ezapparoli/ACC/BAM/target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.bed -g access-excludes.hg19.bed -o acc_antitargets.bed
#cnvkit.py autobin lung14*-2NA*.bam -t /lustre2/scratch/ezapparoli/ACC/BAM/target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.bed -g access-excludes.hg19.bed

cnvkit.py autobin lung14*-2NA*.bam -m amplicon -t /lustre2/scratch/ezapparoli/ACC/BAM/target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.bed
cnvkit.py coverage -p 6 -q 1 lung14FCHM-2NAD114.bam  target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.target.bed -o lung14FCHM-2NAD114.targetcoverage.cnn

#!/bin/bash

script_name=cnv.sh

GENOME=/lustre1/genomes/hg19/fa/hg19.fa
TARGETS=/lustre2/scratch/ezapparoli/ACC/BAM/target_region_WG_IAD97207.20161004.spikeinCL.designedTRIS.bed
ACCESS=/lustre2/scratch/ezapparoli/ACC/BAM/
ANN=/lustre1/genomes/hg19/annotation/hg19.refFlat.txt
BAM=`ls lung14*-2NA*.bam`

cat <<__EOF__> $script_name
#PBS -N CNVBASELINE
#PBS -l select=1:app=java:ncpus=11:mem=18gb
#PBS -P 161
#PBS -q workq
#PBS -m ae

cd $PWD

condactivate


cnvkit.py batch -p 10 --normal `echo $BAM` \
    --targets $TARGETS --fasta $GENOME \
    --access $ACCESS --drop-low-coverage --short-names --annotate $ANN \
    --output-reference acc.hg19.cnn --diagram --scatter --output-dir acc_normal/

condeactivate

__EOF__

qsub $script_name


# b-name folder
cat DNABAM.txt|while read i
do
    R1=$i;
    normal=/lustre2/scratch/Vago/161/Exome/2017/DONOR_BASELINE/hs38DH.cnn
    GENOME=/lustre2/scratch/gbucci/Genome/hs38DH.fa
    TARGETS=/lustre2/scratch/gbucci/Genome/nexterarapidcapture_exome_targetedregions_v1.2_hg38.bed
    ACCESS=/lustre2/scratch/gbucci/Genome/access-10kb.hs38DH.bed
    ANN=/lustre2/scratch/gbucci/Genome/hs38DH.refFlat.txt

    name=${R1%%.bam}
    #name=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level1; done)`
    #lane=`echo $R1| (IFS=_; while read level1 level2 level3 level4 ;do echo $level3; done)`
    lane=1
    echo "$name"
    echo "$GENOME"
    cnvkit.py batch --diagram --scatter \
    -r $normal -p 10 --drop-low-coverage \
    --output-dir b${name}/ $R1
done
