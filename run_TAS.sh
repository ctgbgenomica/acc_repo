

for BAM in *1TAD114.bam
do
    f="$(basename -- $BAM)"
    echo $f "f"
    DIR=${f%.bam} 
    echo $DIR "dir"
    pref=${DIR%%-*} 
    echo $pref "nome"
    mkdir  -p "./$pref"

    #echo $BAM "cio"
    sed "s#reads#${pref}#g" config_TAS.txt  > "config_"$pref".txt"
    sed -i "s#directory#${pref}#g" "config_"$pref".txt"

    /home/amaurizio/Downloads/FREEC-11.5/src/freec -conf "config_"$pref".txt"
done

