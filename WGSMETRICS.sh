#!/bin/bash

#!/bin/bash 
#SBATCH --partition=workq  #workq is the default and unique queue, you do not need to specify.
#SBATCH --account bucci.gabriele
#SBATCH --mem=15GB  # amout of RAM in MB required (and max ram available). Xor "--mem-per-cpu"
#SBATCH --time=INFINITE  ## OR #SBATCH --time=10:00 means 10 minutes OR --time=01:00:00 means 1 hour
#SBATCH --ntasks=6  # nubmer of required cores
#SBATCH --nodes=1  # not really useful for not mpi jobs
#SBATCH --mail-type=FAIL ## BEGIN, END, FAIL or ALL
#SBATCH --mail-user=bucci.gabriele@hsr.it
#SBATCH --error="/home/bucci.gabriele/myjob.err"
#SBATCH --output="/home/bucci.gabriele/myjob.out"
#this script accepts illumina standard fastq names
R1=$1
sleep 3

FA=/beegfs/scratch/ric.cosr/ric.cosr/Menarini/Prove_GAB/reference/hg38.fa
DICT=/beegfs/scratch/ric.cosr/ric.cosr/Menarini/Prove_GAB/reference/hg38.fa.dict
conda activate accwp3

gatk CollectWgsMetricsWithNonZeroCoverage \
       -I $R1 \
       -O ${R1%%.bam}_wgs_metrics.txt \
       -R $FA 



exit

