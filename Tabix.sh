#!/bin/bash
for i in `ls *vcf`
do
	bgzip -f ${i}
	tabix -f -p vcf ${i}.gz
done

