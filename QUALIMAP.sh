#!/bin/bash

#!/bin/bash 
#SBATCH --partition=workq  #workq is the default and unique queue, you do not need to specify.
#SBATCH --account bucci.gabriele
#SBATCH --mem-per-cpu=5GB  # amout of RAM in MB required (and max ram available). Xor "--mem-per-cpu"
#SBATCH --time=INFINITE  ## OR #SBATCH --time=10:00 means 10 minutes OR --time=01:00:00 means 1 hour
#SBATCH --ntasks=12  # nubmer of required cores
#SBATCH --nodes=1  # not really useful for not mpi jobs
#SBATCH --mail-type=FAIL ## BEGIN, END, FAIL or ALL
#SBATCH --mail-user=bucci.gabriele@hsr.it
#SBATCH --error="/home/bucci.gabriele/myjob.err"
#SBATCH --output="/home/bucci.gabriele/myjob.out"
#this script accepts illumina standard fastq names
R1=$1
sleep 3
conda init bash
A=$(conda info --envs)
conda activate accwp3

qualimap bamqc -c -bam $R1 -outdir ${R1%%.bam} -nt 12 -sd -sdmode 1 --java-mem-size=4G 

exit

